#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""
import sys
import socket

# Cliente UDP simple.

try:
    METHOD_SIP = sys.argv[1]                        # Método SIP
    LOGIN = sys.argv[2]
    RECEIVER = LOGIN.split('@')[0]                  # Receptor
    IP = LOGIN.split('@')[1].split(':')[0]          # IP receptor
    PORT = int(LOGIN.split('@')[1].split(':')[1])   # Puerto receptor
except IndexError:
    sys.exit("Usage: python3 client.py method receiver@IP:SIPport")

try:
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((IP, PORT))
        if METHOD_SIP == 'INVITE' or METHOD_SIP == 'BYE':
            LINE = (METHOD_SIP + ' sip:'
                    + LOGIN.split(':')[0] + ' SIP/2.0\r\n')
            print("Enviando: " + LINE)
            my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
        data = my_socket.recv(1024)
        print(data.decode('utf-8'))
        NEW_DATA = data.decode('utf-8').split(" ")
        for element in NEW_DATA:
            if element == '200' and METHOD_SIP != 'BYE':
                LINE = 'ACK sip:' + LOGIN.split(':')[0] + ' SIP/2.0\r\n'
                my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')

        print("Terminando socket...")
    print("Fin.")

except ConnectionRefusedError:
    print("Error de conexión")
