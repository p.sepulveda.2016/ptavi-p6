#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import sys
import socketserver
import os


try:
    IP = sys.argv[1]
    PORT = int(sys.argv[2])
    AUDIO_FILE = sys.argv[3]
except (IndexError, ValueError):
    sys.exit("Usage: python3 server.py IP port audio_file")


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()
            new_line = line.decode('utf-8').split(" ")
            method_sip = new_line[0]
            if not line:
                break       # Si no hay más lineas del bucle infinito
            if method_sip == 'INVITE':
                print("El cliente nos envía " + line.decode('utf-8'))
                self.wfile.write(b"SIP/2.0 100 Trying\r\n\r\n"
                                 + b"SIP/2.0 180 Ringing\r\n\r\n"
                                 + b"SIP/2.0 200 OK\r\n\r\n")
            elif method_sip == 'BYE':
                print("El cliente nos envía " + line.decode('utf-8'))
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            elif method_sip == 'ACK':
                # aEjecutar es un string que se le pasa a una subshell
                AUDIO_FILE = sys.argv[3]
                aEjecutar = './mp32rtp -i 127.0.0.1 -p 23032 < ' + AUDIO_FILE
                print('Vamos a ejecutar', aEjecutar)
                os.system(aEjecutar)
            elif method_sip != ('INVITE' or 'BYE' or 'ACK'):
                self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")
            else:
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")


if __name__ == "__main__":

    # Creamos servidor de eco y escuchamos
    serv = socketserver.UDPServer((IP, PORT), EchoHandler)
    print("Listening...")
    try:
        " Creamos el servidor "
        serv.serve_forever()
    except KeyboardInterrupt:           # Interrumpo servidor por teclado
        print("Finalizado servidor")
